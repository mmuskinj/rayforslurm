#!/usr/bin/env python
"""
A simple test of Ray
"""

import ray

# Import placement group APIs.
from ray.util.placement_group import (
    placement_group,
    placement_group_table,
    remove_placement_group
)

import os
import sys
import time
import argparse


@ray.remote
def f():
    time.sleep(0.01)
    return ray._private.services.get_node_ip_address()


@ray.remote
class actor(object):

    def __init__(self):
        self.ip = ray._private.services.get_node_ip_address()
        print(f"Actor process succsefully initialized at {self.ip}")

    def get_ip(self):
        return self.ip


def main(ip, password):

    # init ray
    ray.init(ignore_reinit_error=True,
             address="%s" % ip, _redis_password="%s" % password)

    # head node ip
    print(f"head node ip: {ray._private.services.get_node_ip_address()}")

    # number of nodes
    nodes = ray.nodes()
    print(f"number of nodes in the cluster: {len(nodes)}")

    # placement groups
    pgs = {}
    for i in range(len(nodes)):
        pg_name = f"pg{i}"
        pgs[pg_name] = placement_group(
            [{"CPU": int(os.environ["RAY_NWORKERS"])}], strategy="STRICT_SPREAD", name=pg_name)

    # Wait until placement group is created.
    print(f"waiting for the placement group creation")
    ray.get([pgs[pg].ready() for pg in pgs])

    # You can look at placement group states using this API.
    print(f"placement group created")
    for pg in pgs:
        print(f"{pg}: {placement_group_table(pgs[pg])}")

    # Execute remote functions 'f' on each of the nodes
    print("Calling the remote function 'f' on each of the nodes.")
    node_ip_addresses = [f.options(placement_group=pgs[f"pg{i}"]).remote() for i in range(len(nodes))]
    print(ray.get(node_ip_addresses))

    # Construct one Actor on each of the nodes
    print("Constructing one Actor per node.")
    actors = []
    for i in range(len(nodes)):
        actors.append(actor.options(placement_group=pgs[f"pg{i}"]).remote())

    # wait for a bit
    time.sleep(0.5)

    # Call the 'get_ip' function of each of the Actors
    actor_ip_addresses = [a.get_ip.remote() for a in actors]
    print(ray.get(actor_ip_addresses))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Wait on ray head node or workers to connect')
    parser.add_argument('--redis-address', default="%s:%s" %
                        (os.environ["RAY_HEAD_IP"], os.environ["RAY_REDIS_PORT"]))
    parser.add_argument('--redis-password',
                        default=os.environ["RAY_REDIS_PASSWORD"])
    args = parser.parse_args()
    main(args.redis_address, args.redis_password)
