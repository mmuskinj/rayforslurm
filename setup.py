from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


from setuptools import find_packages
from setuptools import setup

description = "Distribute HEP jobs with ray"

setup(
    name='rayforslurm',
    version='0.1.0',
    description=description,
    long_description=description,
    author='Miha Muskinja',
    keywords=["ray", "slurm"],
    packages=find_packages(),
    install_requires=[
        "setuptools",
        "ray",
    ],
    setup_requires=[],
    classifiers=[
        "Programming Language :: Python :: 3.8",
    ],
    scripts=[
        'scripts/construct_ray_cluster.sh',
        'scripts/ray_sync.py',
        'scripts/ray_test.py',
    ],
)
